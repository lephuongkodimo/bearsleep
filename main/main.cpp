/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <Audio.h>
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/timer.h"
#include <cstdio>
#include <WiFi.h>
#include "Arduino.h"
#include "BH1750.h"
#include "BLEBear.h"
#include <SHT1x.h>
#include "../mDash/src/mDash.h"
#include "Kalman.h"
#include "WS2812.h"
#include "BearLight.h"
#ifdef MONTE
#include "GSReader.h"
#endif
#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
#define WIFI_NAME "Kodimo"
#define WIFI_PASS "kodimoshtpic"
#define DEVICE_ID "device4"
#define DEVICE_PASS "uWPLahHHKZmkiZ22mOnY3A"
/*define pin i2C sensor*/
#define dataPin  21
#define clockPin 22
#define SDA_LIGHT 19
#define SCL_LIGHT 23
#define PIN_NOISE 34
#ifdef MONTE
#define PIN_WS2812 GPIO_NUM_0
#else
#define PIN_WS2812 GPIO_NUM_18
#endif
#define BIT_LIGHT	( 1 << 0 )
#define BIT_HUMI	( 1 << 1 )
#define BIT_NOISE	( 1 << 2 )
#define BIT_TEMP	( 1 << 3 )
/*define config timer*/
/*
 * event sensor
 */
typedef enum {
 ALARM_READ_LIGHT,
 ALARM_READ_HUMI,
 ALARM_READ_TEMP,
 ALARM_READ_NOISE
}sensor_event_t;
typedef struct {
    sensor_event_t type;  // the type of timer's event
    int timer_group;
    int timer_idx;
    uint64_t timer_counter_value;
} timer_event_t;


timer_idx_t _timer_idx = TIMER_0;
/*variable config time*/
int timeReapeatLightMeter = 1000; //in second min 1000
int timeReapeatTemp = 1000; //in second
int timeReapeatHumi = 1000; //in second
int timeReapeatNoise = 1000; //in second
int CT=5,CL=5,CH=5,CN = 0,MAXC = 5;
static const char *TAG = "MAIN";
/*variable*/
float _temp_c;
 float _temp_f;
 float _humidity;
 float _lux;
 float _db;
 double filter_lux,filter_temp_c,filter_humidity;
 std::string str_;
 //Creates the variables:
Kalman myFilterLight(0.125,32,1023,0); //suggested initial values for high noise filtering
Kalman myFilterTemp(0.125,32,1023,0); //suggested initial values for high noise filtering
Kalman myFilterHumi(0.125,32,1023,0); //suggested initial values for high noise filtering
/*Object*/
SHT1x sht1x(dataPin, clockPin);
BH1750 lightMeter;
Audio Module_Mp3;
WS2812 *ws = new WS2812(PIN_WS2812,NUM_LED);
static BearLight *myLight = new BearLight(ws);
/*RTOS queue*/
xQueueHandle _timer0_queue;
xQueueHandle _ble_queue;
TaskHandle_t TaskHandle_light;
TaskHandle_t TaskHandle_humi;
TaskHandle_t TaskHandle_noise;
TaskHandle_t TaskHandle_temp;
static SemaphoreHandle_t xSemaphoreSensor;
EventBits_t uxBits;
EventGroupHandle_t xEventGroup;

extern "C" {
void app_main(void);
}

void extractIntegerWordsC(std::string str,int *a,int *b,int *c,int *d){
			 char * p ;
			 *a = strtol (str.data(),&p,10);
			  *b = strtol (p,&p,10);
			   *c = strtol (p,&p,10);
			    *d = strtol (p,&p,10);
			    ESP_LOGI(TAG, "->%d %d %d %d",*a,*b,*c,*d);
}

class MyCharBearLightCallback: public BLECharacteristicCallbacks{
	 void onRead(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "BearLight onRead");
	 }
	 void onWrite(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "BearLight onWrite");
		 if(pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_BEAR_LIGHT_SETPIXEL))){
			 ble_message_t mess;
			 mess.evt = ble_evt_t::SETPIXEL;
			 str_ = pCharacteristic->getValue();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 } else if (pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_BEAR_LIGHT_SETHSBPIXEL))){
			 ble_message_t mess;
			 mess.evt = ble_evt_t::SETHSBPIXEL;
			 str_ = pCharacteristic->getValue();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 } else if (pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_BEAR_LIGHT_MOTION))){
			 ble_message_t mess;
			 mess.evt = ble_evt_t::TURN_LIGHT;
			 mess.data = pCharacteristic->getData();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 }

	 }
	 void onNotify(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "BearLight onNotify");
	 }
};
class MyCharSensorCallback: public BLECharacteristicCallbacks{
	 void onRead(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "Sensor onRead");
	 }
	void onWrite(BLECharacteristic* pCharacteristic) {
		ESP_LOGI(TAG, "Sensor Change config");
		if (pCharacteristic->getUUID().equals(
				BLEUUID(CHARACTERISTIC_UUID_SENSOR_LIGHT))) {
			ble_message_t mess;
			mess.evt = CHANGE_CONFIG_ALARM;
			mess.sensor = sensor_t::LIGHT;
			mess.data = pCharacteristic->getData();
			xQueueSend(_ble_queue, &mess, 1000/portTICK_PERIOD_MS);
		} else if (pCharacteristic->getUUID().equals(
				BLEUUID(CHARACTERISTIC_UUID_SENSOR_TEMP))) {
			ble_message_t mess;
			mess.evt = CHANGE_CONFIG_ALARM;
			mess.sensor = sensor_t::TEMP;
			mess.data = pCharacteristic->getData();
			xQueueSend(_ble_queue, &mess, 1000/portTICK_PERIOD_MS);
		} else if (pCharacteristic->getUUID().equals(
				BLEUUID(CHARACTERISTIC_UUID_SENSOR_HUMI))) {
			ble_message_t mess;
			mess.evt = CHANGE_CONFIG_ALARM;
			mess.sensor = sensor_t::HUMI;
			mess.data = pCharacteristic->getData();
			xQueueSend(_ble_queue, &mess, 1000/portTICK_PERIOD_MS);
		} else if (pCharacteristic->getUUID().equals(
				BLEUUID(CHARACTERISTIC_UUID_SENSOR_NOISE))) {
			ble_message_t mess;
			mess.evt = CHANGE_CONFIG_ALARM;
			mess.sensor = sensor_t::NOISE;
			mess.data = pCharacteristic->getData();
			xQueueSend(_ble_queue, &mess, 1000/portTICK_PERIOD_MS);
		}
	}
//	 void onNotify(BLECharacteristic* pCharacteristic){
//		 ESP_LOGI(TAG, "Sensor onNotify");
//	 }
};
class MyCharAudioCallback: public BLECharacteristicCallbacks{
	 void onRead(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "Audio onRead:%d",*pCharacteristic->getData());

	 }
	 void onWrite(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "Audio onWrite");
		 if(pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_AUDIO_PLAY_ID))){
			 ble_message_t mess;
			 mess.evt = AUDIO_PLAY_ID;
			 mess.data = pCharacteristic->getData();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 }else if (pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_AUDIO_PLAY_NAME))){
			 ble_message_t mess;
			 mess.evt = AUDIO_PLAY_NAME;
			 mess.data = pCharacteristic->getData();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 }else if(pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_AUDIO_EVENT))){

			 ble_message_t mess;
			 mess.evt = AUDIO_EVENT;
			 mess.data = pCharacteristic->getData();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 }
	 }
//	 void onNotify(BLECharacteristic* pCharacteristic){
//		 ESP_LOGI(TAG, "Audio onNotify");
//	 }
};
#ifdef MONTE
std::string blockToAudio(GSBlocks bl);
static uint16_t mHendle;
class MyCharCommandCallback: public BLECharacteristicCallbacks{
	 void onWrite(BLECharacteristic* pCharacteristic){
		 ESP_LOGI(TAG, "CMD onWrite");
		 if(pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_BLOCK_ID))){
			 /**/
				int data[6];
				std::string value = pCharacteristic->getValue();
				std::size_t found = value.find("9999");
				if (found != std::string::npos) {
					sscanf(value.c_str(), "%d;%d;", &data[0], &data[1]);
					mHendle = data[1];
				} else {
					if (mHendle == pCharacteristic->getHandle()) {
						/**/
						//HERE
						 GSBlocks mGSBlock;
						 mGSBlock.initBlocks(pCharacteristic->getValue());
						 blockToAudio(mGSBlock);
						 ble_message_t mess;
						 mess.evt = AUDIO_PLAY_NAME;
						 mess.data = (uint8_t*)blockToAudio(mGSBlock).c_str();
						 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
						/**/
					} else {
						printf("Wrong mHendle:%d  getHandle:%d \r\n", mHendle,
								pCharacteristic->getHandle());
					}
				}
			 /**/

		 }else if (pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_PROGRAM))){
			 /**/

						 /**/

		 }else if(pCharacteristic->getUUID().equals(BLEUUID(CHARACTERISTIC_UUID_BLOCK_STATUS))){

			 ble_message_t mess;
			 mess.evt = AUDIO_EVENT;
			 mess.data = pCharacteristic->getData();
			 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
		 }
	 }

};
#endif
static inline void printCoreRuning(char* mtag){
	ESP_LOGI(mtag,"----> running on core  %d",xPortGetCoreID());
}
void configLightFilter(){
}
void configTempFilter(){
	float t = sht1x.readTemperatureC();
	for(int i =0; i < 5 ; i++){
		t+=sht1x.readTemperatureC();
	}
	myFilterTemp.setInitalValue((double)t/6);
}
void configHumiFilter(){
	float h = sht1x.readHumidity();
	for(int i =0; i < 5 ; i++){
		h+=sht1x.readHumidity();
	}
	myFilterHumi.setInitalValue((double)h/6);
}
float ReadLightLever(){
	float l = lightMeter.readLightLevel();
	float fl = l;//(float)myFilterLight.getFilteredValue(l);
//	ESP_LOGI(TAG,"Light:%f",fl);
//	printf("fl=%f ",fl);
	_lux  =fl;
	xEventGroupSetBits(
	                              xEventGroup,    /* The event group being updated. */
	                              BIT_LIGHT);/* The bits being set. */
	return fl;
}
float ReadTemperatureC(){
	float c = sht1x.readTemperatureC();
	float fc = c;//(float)myFilterTemp.getFilteredValue(c);
//	 ESP_LOGI(TAG,"Temp:%f C Raw:%f C",fc,c);
//	printf("fc=%f ",fc);
	_temp_c = fc;
	xEventGroupSetBits(
	                              xEventGroup,    /* The event group being updated. */
	                              BIT_TEMP);/* The bits being set. */
	   return fc;
}
float ReadHumidity(){
	float h = sht1x.readHumidity();
	float fh = h;//(float)myFilterHumi.getFilteredValue(h);
//	ESP_LOGI(TAG,"Humi:%f %% Raw:%f %%",fh,h);
//	printf("fh=%f ",fh);
	_humidity = fh;
	xEventGroupSetBits(
	                              xEventGroup,    /* The event group being updated. */
	                              BIT_HUMI);/* The bits being set. */
	   return fh;
}
float ReadNoise(){
	const int sampleWindow = 50;
	unsigned int sample=0;
	   unsigned long startMillis= millis();                   // Start of sample window
	   float peakToPeak = 0;                                  // peak-to-peak level

	   unsigned int signalMax = 0;                            //minimum value
	   unsigned int signalMin = 1024;                         //maximum value
	   analogSetWidth(10);
	   analogSetPinAttenuation(PIN_NOISE,ADC_11db) ;                                // collect data for 50 mS
	   while (millis() - startMillis < sampleWindow)
	   {
	      sample = analogRead(PIN_NOISE);                             //get reading from microphone
	      if (sample < 1024)                                  // toss out spurious readings
	      {
	         if (sample > signalMax)
	         {
	            signalMax = sample;                           // save just the max levels
	         }
	         else if (sample < signalMin)
	         {
	            signalMin = sample;         	                  // save just the min levels
	         }
	      }
	   }
	   peakToPeak = signalMax - signalMin;                    // max - min = peak-peak amplitude
	   double db = 0.1634*peakToPeak + 1.466;
//	   ESP_LOGI(TAG,"db:%f %% Raw:%f %%",db,peakToPeak);
//	   double volts = (peakToPeak * 3.16) / 1024;  // convert to volts
//	   printf("db=%f ",db);
	   _db = db;
		xEventGroupSetBits(
		                              xEventGroup,    /* The event group being updated. */
		                              BIT_NOISE);/* The bits being set. */
	   return db;
}
void IRAM_ATTR timer_group0_isr(void *para)
{
//	timer_event_t evt;
//	evt.type  = ALARM_READ_LIGHT;
//	xQueueSendFromISR(_timer0_queue, &evt, NULL);
}
static void configTimer(){
	timer_config_t config;
	uint64_t timer_interval_sec = 5;
	config.divider = TIMER_DIVIDER;
	config.counter_dir = TIMER_COUNT_UP;
	config.counter_en = 1;
	config.alarm_en = 1;
	config.intr_type = TIMER_INTR_LEVEL;
	config.auto_reload = 1;
	timer_init(TIMER_GROUP_0, _timer_idx, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(TIMER_GROUP_0, _timer_idx, 0x00000000ULL);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(TIMER_GROUP_0, _timer_idx, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(TIMER_GROUP_0, _timer_idx);
    timer_isr_register(TIMER_GROUP_0, _timer_idx, timer_group0_isr,
        (void *) _timer_idx, ESP_INTR_FLAG_IRAM, NULL);

    timer_start(TIMER_GROUP_0, _timer_idx);
}
static void READ_LIGHT (void *param){
    Wire.begin(SDA_LIGHT,SCL_LIGHT);
    lightMeter.begin();
//    configLightFilter();
	while(1){
		delay(timeReapeatLightMeter);
		myBLEBear.notifyFromSenSor(sensor_t::LIGHT,ReadLightLever());
	}
}
static void READ_TEMP_HUMI_LIGHT (void *param){
//	configTempFilter();
    Wire.begin(SDA_LIGHT,SCL_LIGHT);
    lightMeter.begin();
	int i = 0;
	while(1){
		delay(250);
		i++;
		if(i%CT==0) myBLEBear.notifyFromSenSor(sensor_t::TEMP,ReadTemperatureC());
		if(i%CH==0) myBLEBear.notifyFromSenSor(sensor_t::HUMI,ReadHumidity());
		if(i%CL==0) myBLEBear.notifyFromSenSor(sensor_t::LIGHT,ReadLightLever());
		if(i==MAXC) i = 0;
	}
}

static void READ_TEMP_HUMI (void *param){
//	configHumiFilter();
	while(1){
		delay(timeReapeatHumi);
		myBLEBear.notifyFromSenSor(sensor_t::HUMI,ReadHumidity());
		myBLEBear.notifyFromSenSor(sensor_t::TEMP,ReadTemperatureC());
	}
}
static void READ_NOISE (void *param){
//	configHumiFilter();
	while(1){
		delay(timeReapeatNoise);
		myBLEBear.notifyFromSenSor(sensor_t::NOISE,ReadNoise());
	}
}
void setTimeRepeat(sensor_t s, int t){
	ESP_LOGI(TAG,"Sensor:%d - time set:%d",s,t);
	if(t<0) return;
	if(s==sensor_t::HUMI){
		timeReapeatHumi = t;
		CH = t;
	}else if(s==sensor_t::LIGHT) {
		timeReapeatLightMeter = t;
		CL = t;
	}else if(s==sensor_t::TEMP){
		timeReapeatTemp = t;
		CT = t;
	} else if(s==sensor_t::NOISE){
		timeReapeatNoise = t*500;
		CN = t;
	}
	MAXC= std::max(std::max(CH,CL),CT);
}
void turnLight(bool status){

}
static void timer (void *param){
	timer_event_t evt;
	while(1){
		delay(timeReapeatLightMeter);
		evt.type =ALARM_READ_LIGHT;
		xQueueSend(_timer0_queue, &evt, 1000/portMAX_DELAY);
		evt.type =ALARM_READ_HUMI;
		xQueueSend(_timer0_queue, &evt, 1000/portMAX_DELAY);
		evt.type =ALARM_READ_TEMP;
		xQueueSend(_timer0_queue, &evt, 1000/portMAX_DELAY);
	}
}
static void sensor (void *param){
	ESP_LOGI(TAG,"<<< audio\n");
	/*BH1750 stup*/
    timer_event_t evt;
	   while(1){
		   if(xQueueReceive(_timer0_queue, &evt, portMAX_DELAY)){
			   switch(evt.type){
			   case ALARM_READ_LIGHT:{
				   ReadLightLever();
				   }
				   break;
			   case ALARM_READ_HUMI:{
				   ReadHumidity();
			   }
				   break;
			   case ALARM_READ_TEMP:{
				   ReadTemperatureC();

			   }
				   break;
			   case ALARM_READ_NOISE:{
			   }
				   break;
		   }
		   }
//		   temp_c = sht1x.readTemperatureC();
//		   humidity = sht1x.readHumidity();
//		   ESP_LOGI(TAG,"<<< temp_c:%f\n",temp_c);
//delay(1000);
}
}
static void mywifi (void *param){
	int n = WiFi.scanNetworks();
	for (int i = 0; i < n; ++i) {
	            if(WiFi.SSID(i)==WIFI_NAME){
					if(TaskHandle_light)vTaskSuspend(TaskHandle_light);
					if(TaskHandle_temp)vTaskSuspend(TaskHandle_temp);
					if(TaskHandle_humi) vTaskSuspend(TaskHandle_humi);
					WiFi.begin(WIFI_NAME, WIFI_PASS);
				    while (WiFi.status() != WL_CONNECTED) {
				        delay(500);
				        printf(".");
				    }
				    ESP_LOGI(TAG,"WiFi connected");
				    mDashSetServer("mdash.net", 1883);
					mDashStart(DEVICE_ID, DEVICE_PASS);
	            }
	        }
	vTaskDelete(NULL);
}
static void show(void *param){
	while(1){
		uxBits = xEventGroupWaitBits(
		            xEventGroup,   /* The event group being tested. */
		            BIT_HUMI | BIT_LIGHT | BIT_NOISE | BIT_TEMP, /* The bits within the event group to wait for. */
		            pdTRUE,        /* BIT_0 & BIT_4 should be cleared before returning. */
		            pdTRUE,       /* Don't wait for both bits, either bit will do. */
		            portMAX_DELAY );/* Wait a maximum of 100ms for either bit to be set. */
		  if( ( uxBits & ( BIT_HUMI | BIT_LIGHT | BIT_NOISE | BIT_TEMP ) ) == ( BIT_HUMI | BIT_LIGHT | BIT_NOISE | BIT_TEMP ) )
		  {
//			  printf("_temp_c: %f _humidity:%f _lux:%f _db:%f\n",_temp_c,_humidity,_lux,_db);
		      printf("%f %f %f %f\n",_temp_c,_humidity,_lux/10,_db);
//			  printf("->%d\n",Module_Mp3.getStateAudio());
		  }
	}
}
static void showLight(void *param){
	printCoreRuning("showLight");
	while(1){
			myLight->show();
//			ESP_LOGI(TAG, "Total RAM: %d",ESP.getHeapSize());
//			size_t cap = heap_caps_get_free_size(MALLOC_CAP_8BIT);
//		    ESP_LOGE(TAG, "heap_caps_get_free_size: %d",cap);
//		    delay(2000);
		}
}

static void blebear (void *param){
	printCoreRuning("blebear");
	Module_Mp3.beginAudio();
	Module_Mp3.playMp3File("/sdcard/bst1.mp3");
	ble_message_t mess;
	while(1){
		   if(xQueueReceive(_ble_queue, &mess, portMAX_DELAY)){
			   ESP_LOGI(TAG,"new event:%d",mess.evt);
			   switch(mess.evt){
				case CHANGE_CONFIG_ALARM:
					setTimeRepeat(mess.sensor,*mess.data);
					   break;
				case AUDIO_PLAY_ID:{
					int id = *mess.data;
					ESP_LOGI(TAG,"index:%d",id);
					Module_Mp3.playMp3File(id);
					delay(10);
				}
					break;
				case AUDIO_PLAY_NAME:{
					char* name = (char*)mess.data;
					ESP_LOGI(TAG,"name:%s",name);
					std::string fullURL = "/sdcard/"+std::string(name)+".mp3";
					Module_Mp3.playMp3File(fullURL);
					delay(10);
				}
					break;
				case AUDIO_EVENT:{
					int id = *mess.data;
					ESP_LOGI(TAG,"event:%d",id);
					if(id==AUDIO_CODE_STOP){
						Module_Mp3.stopMp3();
					} else if(id==AUDIO_CODE_RESUME){
						Module_Mp3.resumeMp3();
					}else if(id==AUDIO_CODE_NEXT){
						Module_Mp3.playNextMp3();
					}else if(id==AUDIO_CODE_PAUSE){

						Module_Mp3.pauseMp3();
					} else if(id==0x10){
						vTaskSuspend(TaskHandle_light);
						vTaskSuspend(TaskHandle_temp);
						vTaskSuspend(TaskHandle_humi);
						WiFi.begin(WIFI_NAME, WIFI_PASS);
					    while (WiFi.status() != WL_CONNECTED) {
					        delay(500);
					        printf(".");
					    }
					    ESP_LOGI(TAG,"WiFi connected");
						mDashStart(DEVICE_ID, DEVICE_PASS);
					} else if(id==0x11){
						vTaskResume(TaskHandle_light);
						vTaskResume(TaskHandle_humi);
						vTaskResume(TaskHandle_temp);
					}
				}

									   break;
				case TURN_LIGHT:{
					myLight->setLightState((BearLight::light_t)*mess.data);
				}break;
				case SETHSBPIXEL:{
					int id,r,g,b;
					extractIntegerWordsC(str_,&id,&r,&g,&b);
					myLight->doHSBPixel((uint8_t)id,(uint8_t)r,(uint8_t)g,(uint8_t)b);
				}break;
				case SETPIXEL:{
					int id,r,g,b;
					extractIntegerWordsC(str_,&id,&r,&g,&b);
					myLight->doPixel((uint8_t)id,(uint8_t)r,(uint8_t)g,(uint8_t)b);
				}break;
				default:
					break;
			   }
		   }
	}
}
#ifdef MONTE
std::string blockToAudio(GSBlocks bl){
	std::string song = "";
	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_TURNLEFT){
		song = "tf";
	} else 	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_TURNRIGHT){
		song = "tr";
	} else 	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_MOVEBACKWARD){
		song = "mbw";
	} else 	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_MOVEFORWARD){
		song = "mf";
	} else 	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_START){
		song = "begin";
	} else 	if(bl.whoIam() == GSBlocks::BLOCKS_NAME::_END){
		song = "end";
	}
	return song;
}
class MyReaderCallBacks: public GSReaderCallBacks {
	void onRead(GSBlocks m_GSBlock) {
		ESP_LOGI(TAG, "Readed A Block");
		 ble_message_t mess;
		 mess.evt = AUDIO_PLAY_NAME;
		 mess.data = (uint8_t*) blockToAudio(m_GSBlock).c_str();
		 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
	}
	void onBook(GSBlocks m_GSBlock){

	}
	 void onRobot(BLOCKS_ROBOT_NAME type,BLEAddress address){

	 }

};

static void nfcTask(void *param) {
	GSReader nfcReader;
	nfcReader.init();
	nfcReader.setCallBacks(new MyReaderCallBacks);
	while (1) {
	nfcReader.run();
	}
}
#endif
void app_main()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
    	//
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
	_timer0_queue = xQueueCreate(10, sizeof(timer_event_t));
	_ble_queue = xQueueCreate(10, sizeof(ble_message_t));
	xSemaphoreSensor = xSemaphoreCreateBinary();
	xSemaphoreGive( xSemaphoreSensor );
	xEventGroup = xEventGroupCreate();
	esp_log_level_set("*", ESP_LOG_INFO);
	/*BLE setup*/
	size_t cap = heap_caps_get_free_size(MALLOC_CAP_8BIT); //
    ESP_LOGE(TAG, "heap_caps_get_free_size: %d",cap);
	printf("BEARLIGHT V0.190925 Dev by PIKING\n");
	printCoreRuning("MAIN");
	/*SHT10 setup*/
	myLight->setTimeShow(10);
	myLight->setLightState(BearLight::RED_FADE);
    xTaskCreate(&showLight, "showLight", 2048, NULL, 1, NULL);
    xTaskCreate(&blebear, "blebear", 4096, NULL, 1, NULL);

//    xTaskCreate(&show, "show", 2048, NULL, 1, NULL);

    /*Dung 3 cai nay*/
	xTaskCreate(&READ_NOISE,"READ_NOISE", 2048, NULL, 1, &TaskHandle_noise);
	xTaskCreate(&READ_LIGHT, "READ_LIGHT", 4096, NULL, 1, &TaskHandle_light);
	xTaskCreate(&READ_TEMP_HUMI, "TEMP_HUMI", 4096, NULL, 1, &TaskHandle_temp);



#ifdef MONTE
////	delay(3000);
////	xTaskCreate(&nfcTask, "nfcTask", 4096, NULL, 2,NULL);
//	myBLEBear.setCallbackCommand(new MyCharCommandCallback());
#endif
	myBLEBear.setCallbackSensor(new MyCharSensorCallback());
	myBLEBear.setCallbackBearLight(new MyCharBearLightCallback());
	myBLEBear.setCallbackAudio(new MyCharAudioCallback());
	myBLEBear.start();
}
