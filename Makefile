#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := hello-world
EXTRA_LDFLAGS = -L$(CURDIR)/mDash/src/esp32 -lmDash  # <-- add this line
include $(ADF_PATH)/project.mk
#include $(IDF_PATH)/make/project.mk
