/*
 * BearLight.cpp
 *
 *  Created on: Oct 4, 2019
 *      Author: Admin
 */

#include "BearLight.h"
static SemaphoreHandle_t xSemaphoreLight;
BearLight::BearLight(WS2812 *ws) {
	// TODO Auto-generated constructor stub
	_ws=ws;
	xSemaphoreLight = xSemaphoreCreateBinary();

}

void BearLight::doPixel(uint8_t index, uint8_t red, uint8_t green,
		uint8_t blue) {
	LIGHT_DUTY = false;
	ESP_LOGD("BearLight", "id= %d r= %d  g= %d  b =%d",index,red,green,blue);
	_ws->setPixel(index,red,green,blue);
	_ws->show();
}
void BearLight::doHSBPixel(uint8_t index, uint16_t hue, uint8_t saturation,
		uint8_t brightness) {
	LIGHT_DUTY = false;
	_ws->setHSBPixel(index,hue,saturation,brightness);
	_ws->show();
}
void BearLight::setLightState(BearLight::light_t t){
	if(t<=RED_FADE_ONCE) {
		LIGHT_DUTY = false;
		turnLight(t);
	} else {
		_light_state = t;
		LIGHT_DUTY= true;
	}
}
//ms
void BearLight::setTimeShow(int t){
	_timeShow  = t;
}
void BearLight::turnLight(BearLight::light_t t) {
	switch(t){
	case BearLight::light_t::RED_FADE_ONCE:{
		doRed(_timeShow);
	}break;
	case BearLight::light_t::OFF_ALL:{
		ESP_LOGI("BearLight","OFFFFFFF");
		_lastTimeShow = _timeShow ;
		_timeShow = 5;
		 xSemaphoreTake( xSemaphoreLight, ( TickType_t ) 10000/portTICK_PERIOD_MS );
		_ws->clear();
		_ws->show();
		_timeShow = _lastTimeShow;
		xSemaphoreGive( xSemaphoreLight );
	}break;
	case BearLight::light_t::GREEN_FADE_ONCE:{
		doGreen(_timeShow);
	}break;
	default:
		break;
	}
}

void BearLight::show() {
	if(LIGHT_DUTY){
		xSemaphoreTake( xSemaphoreLight, ( TickType_t ) 1000/portTICK_PERIOD_MS );
		switch(_light_state){
		case BearLight::light_t::GREEN_FADE:{
			doGreen(_timeShow,true);
		}break;
		case BearLight::light_t::RED_FADE:{
			doRed(_timeShow,true);
		}break;
		default:
			vTaskDelay(_timeShow / portTICK_PERIOD_MS);
			break;
		}
		xSemaphoreGive( xSemaphoreLight );
	} else {
		vTaskDelay(_timeShow / portTICK_PERIOD_MS);
	}
}

BearLight::~BearLight() {
	// TODO Auto-generated destructor stub
}
void BearLight::doGreen(int wait,bool useTime){
	for(int k = 0;k < 255;k++){
	for (uint8_t i = 0; i<NUM_LED ;i++){
//		ws->setPixel(i, 0, 255, 0); ???? old ver white
	_ws->setPixel(i, 0, k, 0);
	}
	_ws->show();
	useTime?(vTaskDelay(_timeShow / portTICK_PERIOD_MS)):(vTaskDelay(wait / portTICK_PERIOD_MS));
	}
	for(int k = 254;k >= 0;k--){
	for (uint8_t i = 0; i<NUM_LED ;i++){
//		ws->setPixel(i, 0, 255, 0); ???? old ver white
	_ws->setPixel(i, 0, k, 0);
	}
	_ws->show();
	useTime?(vTaskDelay(_timeShow / portTICK_PERIOD_MS)):(vTaskDelay(wait / portTICK_PERIOD_MS));
	}
}

void BearLight::doRed(int wait,bool useTime){
	for(int k = 0;k < 255;k++){
	for (uint8_t i = 0; i<NUM_LED ;i++){
//		ws->setPixel(i, 0, 255, 0); ???? old ver white
	_ws->setPixel(i, k, 0, 0);
	}
	_ws->show();
	useTime?(vTaskDelay(_timeShow / portTICK_PERIOD_MS)):(vTaskDelay(wait / portTICK_PERIOD_MS));
	}
	for(int k = 254;k >= 0;k--){
	for (uint8_t i = 0; i<NUM_LED ;i++){
//		ws->setPixel(i, 0, 255, 0); ???? old ver white
	_ws->setPixel(i, k, 0, 0);
	}
	_ws->show();
	useTime?(vTaskDelay(_timeShow / portTICK_PERIOD_MS)):(vTaskDelay(wait / portTICK_PERIOD_MS));
	}
}

