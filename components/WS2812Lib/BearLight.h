/*
 * BearLight.h
 *
 *  Created on: Oct 4, 2019
 *      Author: Admin
 */

#ifndef COMPONENTS_WS2812LIB_BEARLIGHT_H_
#define COMPONENTS_WS2812LIB_BEARLIGHT_H_
#include "WS2812.h"
#include <iostream>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "sdkconfig.h"
#include "esp_log.h"

#define NUM_LED 8
/*
 *
 */
class BearLight {
public:
	enum light_t {
		OFF_ALL,
		GREEN_FADE_ONCE,
		RED_FADE_ONCE,//<---------once up, effect below
		RED_FADE,
		GREEN_FADE,
	};
	BearLight(WS2812 *ws);
	void doPixel(uint8_t index, uint8_t red, uint8_t green, uint8_t blue);
	void doHSBPixel(uint8_t index, uint16_t hue, uint8_t saturation, uint8_t brightness);
	void clear();
	void doGreen(int wait,bool useTime = false);
	void doRed(int wait,bool useTime = false);
	void turnLight(BearLight::light_t t);
	void setLightState(BearLight::light_t t);
	void show();
	void setTimeShow(int t);
	virtual ~BearLight();
private:
	WS2812 *_ws;
	bool LIGHT_DUTY = true;
	BearLight::light_t _light_state;
	int _timeShow = 20;
	int _lastTimeShow;
};

#endif /* COMPONENTS_WS2812LIB_BEARLIGHT_H_ */
