/*
 * BLEBear.cpp
 *
 *  Created on: Sep 20, 2019
 *      Author: Admin
 */

#include "BLEBear.h"
static const char *TAG = "BLEBear";
extern xQueueHandle _ble_queue;
BLEServer* pServer = NULL;
/*Profile*/
/***
 */
BLEService *pService_bear_light = NULL;
BLECharacteristic* pCharacteristic_bear_light = NULL;
BLECharacteristic* pCharacteristic_bear_light_setpixel = NULL;
BLECharacteristic* pCharacteristic_bear_light_sethsbpixel = NULL;

BLEService *pService_sensor = NULL;
BLECharacteristic* pCharacteristic_sensor_light = NULL;
BLECharacteristic* pCharacteristic_sensor_temp = NULL;
BLECharacteristic* pCharacteristic_sensor_humi = NULL;
BLECharacteristic* pCharacteristic_sensor_noise = NULL;
BLECharacteristic* pCharacteristic_sensor_light_cf = NULL;
BLECharacteristic* pCharacteristic_sensor_temp_cf = NULL;
BLECharacteristic* pCharacteristic_sensor_humi_cf = NULL;
BLECharacteristic* pCharacteristic_sensor_noise_cf = NULL;

BLEService *pService_audio = NULL;
BLECharacteristic* pCharacteristic_audio_play_id = NULL;
BLECharacteristic* pCharacteristic_audio_play_name = NULL;
BLECharacteristic* pCharacteristic_audio_event = NULL;

BLEService *pService_command = NULL;
BLECharacteristic* pCharacteristic_command_program = NULL;
BLECharacteristic* pCharacteristic_command_block = NULL;
BLECharacteristic* pCharacteristic_command_status = NULL;

BLEService *pService_device = NULL;
BLECharacteristic* pCharacteristic_device_versionfirmware = NULL;
BLECharacteristic* pCharacteristic_device_serialnumber = NULL;
BLECharacteristic* pCharacteristic_device_modelnumber = NULL;

bool deviceConnected = false;
bool oldDeviceConnected = false;
uint32_t value = 0;
BLEBear::BLEBear() {
	// TODO Auto-generated constructor stub

}
BLEBear::~BLEBear() {
	// TODO Auto-generated destructor stub
}
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      ESP_LOGI(TAG, "device Connected");
	 ble_message_t mess;
	 mess.evt = ble_evt_t::AUDIO_PLAY_NAME;
	 char * str ="cnt";
	 mess.data =(uint8_t*) str;
	 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      ESP_LOGI(TAG, "device DisConnected");
	 ble_message_t mess;
	 mess.evt = ble_evt_t::AUDIO_PLAY_NAME;
	 char * str ="dis";
	 mess.data =(uint8_t*) str;
	 xQueueSend(_ble_queue,&mess,1000/portTICK_PERIOD_MS);
    }
};
void BLEBear::start() {
  // Create the BLE Device
  BLEDevice::init("KING");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  //////// Create the BLE Service Light Bear///////////
  pService_bear_light = pServer->createService(SERVICE_UUID_BEAR_LIGHT);
  pCharacteristic_bear_light = pService_bear_light->createCharacteristic(
		  	  	  	  CHARACTERISTIC_UUID_BEAR_LIGHT_MOTION,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
  pCharacteristic_bear_light_setpixel = pService_bear_light->createCharacteristic(
		  CHARACTERISTIC_UUID_BEAR_LIGHT_SETPIXEL,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
  pCharacteristic_bear_light_sethsbpixel = pService_bear_light->createCharacteristic(
		  CHARACTERISTIC_UUID_BEAR_LIGHT_SETHSBPIXEL,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
  pCharacteristic_bear_light->setCallbacks(_pCallbackBearLight);
  pCharacteristic_bear_light_setpixel->setCallbacks(_pCallbackBearLight);
  pCharacteristic_bear_light_sethsbpixel->setCallbacks(_pCallbackBearLight);
  // Start the service
  //////// Create the BLE Service Sensor///////////
  pService_sensor = pServer->createService(SERVICE_UUID_SENSOR);
 //>>>> CHARACTERISTIC SENSOR
  pCharacteristic_sensor_light = pService_sensor->createCharacteristic(
                      CHARACTERISTIC_UUID_SENSOR_LIGHT,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_NOTIFY|
					  BLECharacteristic::PROPERTY_WRITE
                    );
  pCharacteristic_sensor_humi = pService_sensor->createCharacteristic(
                      CHARACTERISTIC_UUID_SENSOR_HUMI,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_NOTIFY|
					  BLECharacteristic::PROPERTY_WRITE
                    );
  pCharacteristic_sensor_temp = pService_sensor->createCharacteristic(
                      CHARACTERISTIC_UUID_SENSOR_TEMP,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_NOTIFY|
					  BLECharacteristic::PROPERTY_WRITE
                    );
  pCharacteristic_sensor_noise = pService_sensor->createCharacteristic(
                      CHARACTERISTIC_UUID_SENSOR_NOISE,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_NOTIFY|
					  BLECharacteristic::PROPERTY_WRITE
                    );
  //>>> CONFIG SENSOR
//  pCharacteristic_sensor_light_cf = pService_sensor->createCharacteristic(
//                       CHARACTERISTIC_UUID_SENSOR_LIGHT_CF,
//                       BLECharacteristic::PROPERTY_READ   |
//                       BLECharacteristic::PROPERTY_WRITE
//                     );
//   pCharacteristic_sensor_humi_cf = pService_sensor->createCharacteristic(
//                       CHARACTERISTIC_UUID_SENSOR_HUMI_CF,
//                       BLECharacteristic::PROPERTY_READ   |
//                       BLECharacteristic::PROPERTY_WRITE
//                     );
//   pCharacteristic_sensor_temp_cf = pService_sensor->createCharacteristic(
//                       CHARACTERISTIC_UUID_SENSOR_TEMP_CF,
//                       BLECharacteristic::PROPERTY_READ   |
//                       BLECharacteristic::PROPERTY_WRITE
//                     );
//   pCharacteristic_sensor_noise_cf = pService_sensor->createCharacteristic(
//                       CHARACTERISTIC_UUID_SENSOR_NOISE_CF,
//                       BLECharacteristic::PROPERTY_READ   |
//                       BLECharacteristic::PROPERTY_WRITE
//                     );

   // Create a BLE Descriptor
   pCharacteristic_sensor_light->addDescriptor(new BLE2902());
   pCharacteristic_sensor_humi->addDescriptor(new BLE2902());
   pCharacteristic_sensor_temp->addDescriptor(new BLE2902());
   pCharacteristic_sensor_noise->addDescriptor(new BLE2902());

   pCharacteristic_sensor_humi->setCallbacks(_pCallbackSensor);
   pCharacteristic_sensor_light->setCallbacks(_pCallbackSensor);
   pCharacteristic_sensor_temp->setCallbacks(_pCallbackSensor);
   pCharacteristic_sensor_noise->setCallbacks(_pCallbackSensor);

//   pCharacteristic_sensor_humi_cf->setCallbacks(_pCallbackSensor);
//   pCharacteristic_sensor_light_cf->setCallbacks(_pCallbackSensor);
//   pCharacteristic_sensor_temp_cf->setCallbacks(_pCallbackSensor);
//   pCharacteristic_sensor_noise_cf->setCallbacks(_pCallbackSensor);
   //<<<< CONFIG SENSOR
   //<<<< CHARACTERISTIC SENSOR
  // Start the service
  //////// Create the BLE Service AUDIO///////////
    pService_audio = pServer->createService(SERVICE_UUID_AUDIO);
    pCharacteristic_audio_play_id = pService_audio->createCharacteristic(
                        CHARACTERISTIC_UUID_AUDIO_PLAY_ID,
                        BLECharacteristic::PROPERTY_READ   |
                        BLECharacteristic::PROPERTY_WRITE
                      );
    pCharacteristic_audio_play_name = pService_audio->createCharacteristic(
                            CHARACTERISTIC_UUID_AUDIO_PLAY_NAME,
                            BLECharacteristic::PROPERTY_READ   |
                            BLECharacteristic::PROPERTY_WRITE
                          );
    pCharacteristic_audio_event = pService_audio->createCharacteristic(
                        CHARACTERISTIC_UUID_AUDIO_EVENT,
                        BLECharacteristic::PROPERTY_READ   |
                        BLECharacteristic::PROPERTY_WRITE
                      );

    pCharacteristic_audio_play_id->setCallbacks(_pCallbackAudio);
    pCharacteristic_audio_play_name->setCallbacks(_pCallbackAudio);
    pCharacteristic_audio_event->setCallbacks(_pCallbackAudio);


    //////// Create the BLE Service COMMNAD///////////
       pService_command = pServer->createService(SERVICE_UUID_COMMAND);
       pCharacteristic_command_block = pService_command->createCharacteristic(
    		   CHARACTERISTIC_UUID_BLOCK_ID,
                           BLECharacteristic::PROPERTY_WRITE
                         );
       pCharacteristic_command_program = pService_command->createCharacteristic(
    		   CHARACTERISTIC_UUID_PROGRAM,
                               BLECharacteristic::PROPERTY_WRITE
                             );
       pCharacteristic_command_status = pService_command->createCharacteristic(
    		   CHARACTERISTIC_UUID_BLOCK_STATUS,
                           BLECharacteristic::PROPERTY_WRITE
                         );

       pCharacteristic_command_block->setCallbacks(_pCallbackCommand);
       pCharacteristic_command_program->setCallbacks(_pCallbackCommand);
       pCharacteristic_command_status->setCallbacks(_pCallbackCommand);

       //////// Create the BLE Service Device///////////
          pService_device = pServer->createService(UUID_MONTE_DEVICE_INFORMATION);
          pCharacteristic_device_modelnumber = pService_device->createCharacteristic(
        		  UUID_MODEL_NUMBER,
                              BLECharacteristic::PROPERTY_READ
                            );
//          pCharacteristic_device_serialnumber = pService_device->createCharacteristic(
//        		  UUID_SERIAL_NUMBER,
//                                  BLECharacteristic::PROPERTY_READ
//                                );
//          pCharacteristic_device_versionfirmware = pService_device->createCharacteristic(
//        		  UUID_FIRMWARE_VERSION,
//                              BLECharacteristic::PROPERTY_READ
//                            );

          pCharacteristic_device_modelnumber->setCallbacks(_pCallbackDevice);
//          pCharacteristic_device_serialnumber->setCallbacks(_pCallbackDevice);
//          pCharacteristic_device_versionfirmware->setCallbacks(_pCallbackDevice);
          pCharacteristic_device_modelnumber->setValue("AU-01/VN");


       // Start the service
       pService_bear_light->start();
       pService_sensor->start();
       pService_audio->start();
       pService_device->start();
#ifdef MONTE
       pService_command->start();
#endif
       //<<audio

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID_ADV);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  ESP_LOGI(TAG, "<<Start advertis");
}
BLEBear myBLEBear;

void BLEBear::setCallbackCommand(BLECharacteristicCallbacks* pCallbacks) {
	_pCallbackCommand = pCallbacks;
}
void BLEBear::setCallbackAudio(BLECharacteristicCallbacks* pCallbacks) {
	_pCallbackAudio = pCallbacks;
}

void BLEBear::setCallbackSensor(BLECharacteristicCallbacks* pCallbacks) {
	_pCallbackSensor  = pCallbacks;
}

void BLEBear::setCallbackBearLight(BLECharacteristicCallbacks* pCallbacks) {
	_pCallbackBearLight  = pCallbacks;
}
void BLEBear::setCallbackDevice(BLECharacteristicCallbacks* pCallbacks) {
	_pCallbackDevice  = pCallbacks;
}

void BLEBear::notifyFromSenSor(sensor_t s,float v) {
#ifdef NOTIFY_BLE
	if(deviceConnected){
	switch(s){
	case LIGHT:
		pCharacteristic_sensor_light->setValue(v);
		pCharacteristic_sensor_light->notify();
		break;
	case TEMP:
		pCharacteristic_sensor_temp->setValue(v);
		pCharacteristic_sensor_temp->notify();
		break;
	case HUMI:
		pCharacteristic_sensor_humi->setValue(v);
		pCharacteristic_sensor_humi->notify();
		break;
	case NOISE:
		pCharacteristic_sensor_noise->setValue(v);
		pCharacteristic_sensor_noise->notify();
		break;
	default:
		break;
	}
	}
#endif
}
