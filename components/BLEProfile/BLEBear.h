/*
 * BLEBear.h
 *
 *  Created on: Sep 20, 2019
 *      Author: Piking
 */

#ifndef COMPONENTS_BLEPROFILE_BLEBEAR_H_
#define COMPONENTS_BLEPROFILE_BLEBEAR_H_
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include "esp_log.h"
#define NOTIFY_BLE
//#define MONTE

/*Base Service*/
#define SERVICE_UUID_BASE        "131e0000-3b84-4719-8781-221086b1f1fb"
#define SERVICE_UUID_ADV        SERVICE_UUID_BASE //<- advertisement

/*Management color effect of led RGB*/
#define SERVICE_UUID_BEAR_LIGHT        "131EAF8C-3B84-4719-8781-221086B1F1FB" //<- service
#define CHARACTERISTIC_UUID_BEAR_LIGHT_SETPIXEL "131E3B36-3B84-4719-8781-221086B1F1FB" //<- string: "LED R G B" ex: "3 255 0 0 0"
#define CHARACTERISTIC_UUID_BEAR_LIGHT_SETHSBPIXEL "131E942C-3B84-4719-8781-221086B1F1FB"
#define CHARACTERISTIC_UUID_BEAR_LIGHT_MOTION "131E80DD-3B84-4719-8781-221086B1F1FB" //<- selection index motion effect

/*Communication with sensor*/
#define SERVICE_UUID_SENSOR        "131E9F84-3B84-4719-8781-221086B1F1FB"
#define CHARACTERISTIC_UUID_SENSOR_LIGHT "131E10CC-3B84-4719-8781-221086B1F1FB" //<-notify data to mobile app ; write T to config time read sensor T*250 ms
#define CHARACTERISTIC_UUID_SENSOR_TEMP "131E24E3-3B84-4719-8781-221086B1F1FB" //<-notify data to mobile app ; write to config time read sensor
#define CHARACTERISTIC_UUID_SENSOR_HUMI "131EBBA6-3B84-4719-8781-221086B1F1FB"//<-notify data to mobile app ; write to config time read sensor
#define CHARACTERISTIC_UUID_SENSOR_NOISE "131E070E-3B84-4719-8781-221086B1F1FB"//<-notify data to mobile app ; write to config time read sensor


/*Audio*/
#define SERVICE_UUID_AUDIO        "131EB226-3B84-4719-8781-221086B1F1FB"
#define CHARACTERISTIC_UUID_AUDIO_PLAY_ID "131EC071-3B84-4719-8781-221086B1F1FB" //<-Play song in sdcard with index
#define CHARACTERISTIC_UUID_AUDIO_PLAY_NAME "131E0E42-3B84-4719-8781-221086B1F1FB" //<-Play song in sdcard with name
#define CHARACTERISTIC_UUID_AUDIO_EVENT "131E7486-3B84-4719-8781-221086B1F1FB" //<-follow audio code


#define AUDIO_CODE_STOP 0x01
#define AUDIO_CODE_RESUME 0x02
#define AUDIO_CODE_NEXT 0x03
#define AUDIO_CODE_PAUSE 0x04

#define SERVICE_UUID_COMMAND		"6261CB01-F5EA-490E-8548-E6821E3E7792"
#define CHARACTERISTIC_UUID_BLOCK_ID 				"62614C14-F5EA-490E-8548-E6821E3E7792"
#define CHARACTERISTIC_UUID_PROGRAM 				"6261EC06-F5EA-490E-8548-E6821E3E7792"
#define CHARACTERISTIC_UUID_BLOCK_STATUS				"6261C024-F5EA-490E-8548-E6821E3E7792"

//************profile MONTE DEVICE INFORMATION****************//
#define UUID_MONTE_DEVICE_INFORMATION 	"6261A894-F5EA-490E-8548-E6821E3E7792"
#define UUID_FIRMWARE_VERSION 			"62610EEA-F5EA-490E-8548-E6821E3E7792"
#define UUID_HARDWARE_VERSION 			"62618B98-F5EA-490E-8548-E6821E3E7792"
#define UUID_SERIAL_NUMBER 				"6261FC91-F5EA-490E-8548-E6821E3E7792"
#define UUID_MODEL_NUMBER 				"626156A5-F5EA-490E-8548-E6821E3E7792"


/*
 * type sensor
 */
typedef enum {
		LIGHT,
		TEMP,
		HUMI,
		NOISE

} sensor_t;
typedef enum {
 CHANGE_CONFIG_ALARM,
 CHANGE_CONFIG_ALARM_LIGHT,
 CHANGE_CONFIG_ALARM_HUMI,
 CHANGE_CONFIG_ALARM_TEMP,
 AUDIO_VOLUM_UP,
 AUDIO_VOLUM_DOWN,
 AUDIO_PLAY_ID,
 AUDIO_PLAY_NAME,
 AUDIO_PLAY_PROGRAM,
 AUDIO_EVENT,
 TURN_LIGHT,
 SETPIXEL,
 SETHSBPIXEL,
 CONNECTED,
 DISCONENCTED
} ble_evt_t;
typedef struct {
    ble_evt_t evt;  // the type of timer's event
    sensor_t sensor;
    uint8_t *data;
} ble_message_t;
class BLEBear {
public:
	BLEBear();
	virtual ~BLEBear();
	void init();
	void start();
	void setCallbackCommand(BLECharacteristicCallbacks* pCallbacks);
	void setCallbackAudio(BLECharacteristicCallbacks* pCallbacks);
	void setCallbackSensor(BLECharacteristicCallbacks* pCallbacks);
	void setCallbackBearLight(BLECharacteristicCallbacks* pCallbacks);
	void setCallbackDevice(BLECharacteristicCallbacks* pCallbacks);
	void notifyFromSenSor(sensor_t s, std::string v);
	void notifyFromSenSor(sensor_t s, float v);
	void notifyFromSenSor(sensor_t s, int v);
private:
	BLECharacteristicCallbacks* _pCallbackCommand;
	BLECharacteristicCallbacks* _pCallbackAudio;
	BLECharacteristicCallbacks* _pCallbackSensor;
	BLECharacteristicCallbacks* _pCallbackBearLight;
	BLECharacteristicCallbacks* _pCallbackDevice;
};
extern BLEBear myBLEBear;
#endif /* COMPONENTS_BLEPROFILE_BLEBEAR_H_ */
