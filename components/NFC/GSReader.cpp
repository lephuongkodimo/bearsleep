/*
 * GSReader.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSReader.h"
static TaskHandle_t buzzerBook;
bool isBuzzerNow = false;
SemaphoreHandle_t xSemaphoreBuzzerDoneTone = NULL;//queue handle nfc read done note_t block
//GSReader::GSReader() {
//	// TODO Auto-generated constructor stub
//}
//static void buzzerBookTask(void*param){
//	while(1){
////		GSg_SPK.play(song_t::soundbook);
//		if ( xSemaphoreTake(xSemaphoreBuzzerDoneTone,(TickType_t ) 0) == pdTRUE) {
//								//nfc give a semaphore to stop buzzer
//								//if nfc not give semaphore, stop buzzer after 4s, and wait until out nfc tag
//								vTaskDelete(NULL);
//								break;
//							}
//	}
//}
//static dataBook_t bookLesson;

GSReader::~GSReader() {
	// TODO Auto-generated destructor stub
}
void GSReader::write(std::string str, uint8_t blockNumber, uint8_t *uid,
		uint8_t uidLength) {
	uint8_t dataBlock[16];
	memset(dataBlock, 0, 16);
	uint8_t data[16];
	strcpy((char*) dataBlock, str.c_str());
	memcpy(data, dataBlock, sizeof data);
	uint8_t success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,blockNumber, 0, _Keya);

	unsigned char config[] = {0xFF, 0x07, 0x80, 0x69};
	uint8_t numberSector = 0;
	if(!success){
		success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
		success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,blockNumber, 0, _KeyaOld);
		ESP_LOGE(REA_TAG, "Try old key write"); // @suppress("Invalid arguments")
		if(success){
			numberSector =  blockNumber/4;
			ESP_LOGE(REA_TAG, "numberSector %d",numberSector); // @suppress("Invalid arguments")
		}
	}

	success = nfc.mifareclassic_WriteDataBlock(blockNumber, data);
	if (success) {
		ESP_LOGI(REA_TAG, "Done write : %s", str.c_str());
	} else {
		ESP_LOGE(REA_TAG, "Erro write");
	}

	if(numberSector!=0){
		blockNumber = numberSector*4+3;
	      for (int i = 0; i < 6; i++) {
	        data[i] = _Keya[i];
	        data[i+ 10] = _KeyaOld[i];
	      }

	      for (int i = 0; i < 4 ; i++) {
	        data[i + 6] = config[i];
	      }
		success = nfc.mifareclassic_WriteDataBlock(blockNumber, data);
		if (success) {
			ESP_LOGI(REA_TAG, "Done write new _keyA block %d",blockNumber); // @suppress("Invalid arguments")
		} else {
			ESP_LOGE(REA_TAG, "Erro write new _KeyA %d",blockNumber); // @suppress("Invalid arguments")
		}
	}

}

void GSReader::init() {
	delay(111);
	nfc.begin();
	uint32_t versiondata = nfc.getFirmwareVersion();
	for(int i =0 ; i < 5 ;i++){
	versiondata = nfc.getFirmwareVersion();
	if (!versiondata) {
		//future add warring in here
		ESP_LOGI(REA_TAG, "Didn't find PN53x board %d\n",i);
		if(i==4) while (1)
			; // halt
	}
	}
	// Got ok data, print it out!
	ESP_LOGI(REA_TAG, "Found chip PN5");
	// configure board to read RFID tags
//		nfc.setPassiveActivationRetries(0xFF);
	nfc.SAMConfig();
	Serial.println("Waiting for an ISO14443A Card ...");
	xSemaphoreBuzzerDoneTone = xSemaphoreCreateBinary();
}
std::string GSReader::read(uint8_t *uid, uint8_t uidLength) {
	std::string str = "";

	uint8_t blockNumber = 4;
	str = GSReader::readBlock(uid, uidLength, blockNumber);
//	blockNumber = 5 ;
//	str = str + GSReader::readBlock(uid,uidLength,blockNumber);
	return str;
}
std::string GSReader::readBlock(uint8_t *uid, uint8_t uidLength,uint8_t blockNumber) {
	std::string str = "";
// We probably have a Mifare Classic card ...
	ESP_LOGD(REA_TAG,"Seems to be a Mifare Classic card (4 byte UID)");

// Now we need to try to authenticate it for read/write access
// Try with the factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
	ESP_LOGD(REA_TAG,"Trying to authenticate block 4 with default KEYA value");
//	uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; //{ 0x25, 0x09, 0x95, 0x4B, 0x44, 0x4D };
// Start with block 4 (the first block of sector 1) since sector 0
// contains the manufacturer data and it's probably better just
// to leave it alone unless you know what you're doing
//	uint8_t success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,blockNumber, 0, keya);

	uint8_t success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,blockNumber, 0, _Keya);
	if(!success){
		success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
		success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,blockNumber, 0, _KeyaOld);
		ESP_LOGI(REA_TAG,"Trying Old Key ");
	}
	if (success) {
		ESP_LOGD(REA_TAG,"Sector 1 (Blocks 4..7) has been authenticated");
		uint8_t data[16];
		std::stringstream sd;
		// If you want to write something to block 4 to test with, uncomment
		// the following line and this text should be read back in a minute
		//					memcpy(data, (const uint8_t[]){ 'a', 'd', 'a', 'f', 'r', 'u', 'i', 't', '.', 'c', 'o', 'm', 0, 0, 0, 0 }, sizeof data);
		//					success = nfc.mifareclassic_WriteDataBlock (4, data);

		// Try to read the contents of block 4
		success = nfc.mifareclassic_ReadDataBlock(blockNumber, data);
		if (success) {
			sd << data;
			std::string sdata = sd.str();
			str = sdata;
			// Data seems to have been read ... spit it out
//			Serial.printf("Reading Block: %d\r\n",blockNumber);
//			nfc.PrintHexChar(data, 16);
//			ESP_LOGI(REA_TAG, "STRING: %s", sdata.c_str());
//			Serial.println("");

			// Wait a bit before reading the card again
//			Serial.println("delay(1000);");

		} else {
			Serial.println(
					"Ooops ... unable to read the requested block.  Try another key?");
		}
	} else {
		Serial.println("Ooops ... authentication failed: Try another key?");
	}
	return str;
}
/*
 * is gsblock
 */
bool GSReader::isBlocks(std::string strTag) {
	int index = strTag.find("(");
	if (index != 0)
		return false;
	index = strTag.find(")");
	if (index <= 0)
		return false;
	return true;

}

byte cmd_buffer[34];
byte mac_buffer[34];
std::string writeStore = "";
int countScan =0;
void GSReader::run() {
	uint8_t success;
	uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 }; // Buffer to store the returned UID
	uint8_t last_uid[] = { 0, 0, 0, 0, 0, 0, 0 }; // Buffer to store the returned UID
	uint8_t uidLength; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
//	unsigned dataArraySize = sizeof(uid) / sizeof(uint8_t);
	// Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
	// 'uid' will be populated with the UID, and uidLength will indicate
	// if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
	success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
	if (success) {
		/***************copmapre twwo 2 tag for ver1************/
		memcpy(last_uid,uid,sizeof(uid));
//		vuid.clear();
//		/*
//		 * help we no duplicate block
//		 */
//		copy(&uid[0], &uid[dataArraySize], back_inserter(vuid));
//		if (vuid_last == vuid) {
//			cdetec++;
//			if (cdetec == 7) {
//				vuid_last.clear();
//				cdetec = 0;
//			}
//			return;
//		}
//		vuid_last.clear();
//		vuid_last = vuid;
		/****************end comapre ********************/
		// Display some basic information about the card
		ESP_LOGI(REA_TAG, "Found an ISO14443A card");
//		Serial.print("  UID Value: ");
//		nfc.PrintHex(uid, uidLength);
		if (uidLength == 4) {
//			********************************************//
#ifdef VDEV
			if(GSg_testTag){
				GSBlocks tmpGSBlock = GSBlocks("(210,0,0)");
				mGSReaderCallBacks->onRead(tmpGSBlock);
				 while (nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength,50))
				 {
					 /*trong vung tu truong nhung khac uid*/
					 if(strcmp((char*)uid,(char*)last_uid)!=0){
						 break;
					 }
					 //TODO when do nothing when recive nfc
				 }
				 return;
			}
			if (GSg_writeTag) {

				Serial.setTimeout(500L); // wait until 20 seconds for input from serial
				// Ask to input command for this tag
				ESP_LOGI(REA_TAG, "1 - Begin \n2 - End \n3 - Repeat Forever\n4- Move Forward \n5 - Move Backward\n6 Turn Left\n7 - Turn Right"); // @suppress("Invalid arguments")
//				Serial.print(F("Type your command, ending with #"));
				byte len;// = Serial.readBytesUntil('#', (char *) cmd_buffer, 30); // read family name from serial
				len = Serial.readBytes((char *) cmd_buffer,30);
//					len = Serial.readBytesUntil('#', (char *) cmd_buffer, 30); // read family name from serial
				for (byte i = len; i < 30; i++)
					cmd_buffer[i] = '\0';     // pad with spaces

				std::string tmpTag = std::string((char*) cmd_buffer);
				ESP_LOGI("NFC", "Selected:'%s'",tmpTag.c_str());
				if(tmpTag=="r") {
					memset(cmd_buffer,0,34);
					memset(mac_buffer,0,34);
					return;
				} else if(tmpTag=="1"){
					writeStore = "(100)";
				} else if(tmpTag=="2"){
					writeStore = "(1000)";
				} else if(tmpTag=="3"){
					writeStore = "(1001)";
				} else if(tmpTag=="4"){
					writeStore = "(200,15,-1)";
				} else if(tmpTag=="5"){
					writeStore = "(200,-15,-1)";
				} else if(tmpTag=="6"){
					writeStore = "(210,90)";
				} else if(tmpTag=="7"){
					writeStore = "(210,-90)";
				} else if(tmpTag=="a"){
					writeStore = "(301,6)";
				} else if(tmpTag=="b"){
					writeStore = "(301,7)";
				} else if(tmpTag=="c"){
					writeStore = "(301,1)";
				} else if(tmpTag=="d"){
					writeStore = "(301,2)";
				} else if(tmpTag=="e"){
					writeStore = "(301,3)";
				} else if(tmpTag=="f"){
					writeStore = "(301,4)";
				} else if(tmpTag=="g"){
					writeStore = "(301,5)";
				} else if(tmpTag=="h"){
					writeStore = "(301,8)";
				} else if(tmpTag=="w"){
					Serial.setTimeout(40000L); // wait until 20 seconds for input from serial
					Serial.println(F("Type your ID Wooody, ending with #"));
					len = Serial.readBytesUntil('#', (char *) cmd_buffer, 30); // read family name from serial
					for (byte i = len; i < 30; i++)
										cmd_buffer[i] = '\0';     // pad with spaces
									ESP_LOGI("NFC", "Robot . . .");

									writeStore = std::string((char*) cmd_buffer);
				} else {
					ESP_LOGI("NFC", "Press again");
					return;
				}
				ESP_LOGI("NFC", "is writing . . .'%s'",writeStore.c_str());
				if (!isBlocks(writeStore))
					return;
				this->write(writeStore, 4, uid, uidLength);
				GSBlocks tmpGSBlock;
				tmpGSBlock.initBlocks(writeStore);//Tao 1 GSBlock moi phat hien duoc
				if (tmpGSBlock.whoIam() == GSBlocks::BLOCKS_NAME::_ROBOT) {
					// Ask to input command for this tag
					Serial.println(F("Type your BLE Address, ending with #"));
					len = Serial.readBytesUntil('#', (char *) mac_buffer, 30); // read family name from serial
					for (byte i = len; i < 30; i++)
						mac_buffer[i] = '\0';     // pad with spaces
					ESP_LOGI("NFC", "BLE Address is writing . . .");
					this->write(std::string((char*) mac_buffer), 5, uid,uidLength);
				}
			}
#endif //vdev

			//(0,T,0001)#
			//(240ac4a2299a)# csgt
			//(0,G,0001)#(b4e62db5ac07)# HTV
			//(0,G,0003)#(30:ae:a4:02:9c:06)# HTV step
			//(0,G,0002)
			//Monte App
			//(0,A,0001)
			//(72211a79675d)
			//(0,G,0000004)
			//(240ac4a228d2)

			//5: (0,G,0000005)# (cc50e3a88396)#
			//(0,G,0000006)# (240ac4a228ba)#
			//(0,G,0000007)# (30AEA4029E76)# iotmaker board
			// (0,G,0000008)# (B4E62DC1F5FB)#
			//(0,G,0000009)# (30AEA40271CE)#
			//(0,G,0000010)# (cc50e39594a2)#
//			*******************************************//


			std::string strTag = read(uid, uidLength);
//			ESP_LOGI(REA_TAG, "String read: '%s'", strTag.c_str());

			if (!isBlocks(strTag))
				return;
			GSBlocks mGSBlock;
			mGSBlock.initBlocks(strTag);	//Tao 1 GSBlock moi phat hien duoc
			if (mGSReaderCallBacks) {
				if (mGSBlock.whoIam() == GSBlocks::BLOCKS_NAME::_ROBOT) {
					std::string RobotAddressStr = readBlock(uid, uidLength, 5);
					ESP_LOGI(REA_TAG, "BLE Address read:'%s'", RobotAddressStr.c_str());
					if (!isBlocks(RobotAddressStr))	return;
					GSBlocks RobotAddress;
					RobotAddress.initBlocks(RobotAddressStr);
					mGSReaderCallBacks->onRobot(mGSBlock.getRobotName(),RobotAddress.toBLEAdress());
				} else if(mGSBlock.whoIam() == GSBlocks::BLOCKS_NAME::_DATA){
						std::string data="";
						for(uint8_t i = 1 ; i < 16 ; i++){
							for(uint8_t k = 0 ; k < 3;k++){
								std::string tmpData = readBlock(uid, uidLength, i*4+k);
								std::size_t found = data.find("KODIMO");
								data+=tmpData;
								if (found!=std::string::npos){
									mGSBlock.setData(data);
									break;
								}
							}

						}
					} else if(mGSBlock.whoIam() == GSBlocks::BLOCKS_NAME::_BOOK){
#ifdef USING_BOOK
//						xTaskCreate(&buzzerBookTask, "buzzer", 2048, NULL, 2, &buzzerBook);
						std::string addressBlock = readBlock(uid, uidLength, 5);
						if (!isBlocks(addressBlock))return;
						GSBlocks AddressBlock;
						AddressBlock.initBlocks(addressBlock);
						int nbStory = textToInteger(AddressBlock.getElement(1));
						int nbLesson = textToInteger(AddressBlock.getElement(2));
						int nbSolution = textToInteger(AddressBlock.getElement(3));
						ESP_LOGI(REA_TAG, "nbStory:%d nbLesson:%d nbSolution:%d",nbStory,nbLesson,nbSolution);
						getBookFromBlock(nbStory,uid, uidLength);
						getBookFromBlock(nbLesson,uid, uidLength);
						getBookFromBlock(nbSolution,uid, uidLength);

						ESP_LOGD(REA_TAG, "START-END-ENEMY-%d\n",count_data_bool);
						if(count_data_bool==3) {
						MTBook::getBook()->setUnit(mGSBlock.getElement(1));
						MTBook::getBook()->getStart().dump();
						MTBook::getBook()->getEnd().dump();
						MTBook::getBook()->getEnemy().dump();
						printf("\n");
						mGSReaderCallBacks->onBook(mGSBlock);
						} else {
							ESP_LOGI(REA_TAG, "Fail read book\n");
						}
						count_data_bool=0;
//						xSemaphoreGive( xSemaphoreBuzzerDoneTone );
#endif
					}
					else {
						mGSReaderCallBacks->onRead(mGSBlock);
				}	//Callback
			}
			 while (nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength,50))
			 {
				 /*trong vung tu truong nhung khac uid*/
				 if(strcmp((char*)uid,(char*)last_uid)!=0){
					 break;
				 }
				 //TODO when do nothing when recive nfc
			 }
			if (mGSBlock.whoIam() == GSBlocks::BLOCKS_NAME::_MUSIC_NOTE) {
					if(isBuzzerNow) {
						isBuzzerNow=false;
						ESP_LOGI(REA_TAG, "Give semaphore Buzzer stop");
						xSemaphoreGive( xSemaphoreBuzzerDoneTone );
					}
			}
			countScan++;
			ESP_LOGI(REA_TAG, "countScan:%d",countScan);
		}


	}
//	else {
//		Serial.println("Timed out waiting for a card");
//	}

}
#ifdef USING_BOOK
//void GSReader::getBookFromBlock(int nbStart,uint8_t *uid, uint8_t uidLength){
//	int header;
//	std::string dataBlock = getBookData(nbStart,uid, uidLength);
////	ESP_LOGI(REA_TAG, "dataBlock:%s",dataBlock.c_str());
//	std::string str =  removeHeader(header,dataBlock);
//	ESP_LOGI(REA_TAG, "Header = %d, data:%s",header,str.c_str());
//	if(header==511){
//		MTBook::getBook()->setStory(str);
//		count_data_bool++;
//	} else if(header==512) {
//		MTBook::getBook()->setLesson(str);
//		count_data_bool++;
//	} else if(header==513) {
//		count_data_bool++;
//		MTBook::getBook()->setSolution(str);
//	}
//
//}
#endif
std::string GSReader::getBookData(int nbStart,uint8_t *uid, uint8_t uidLength){
	// "(132,132131313521)))"
	return  readBlockUltil(")))",nbStart,uid, uidLength);
}
////(512,{}))) -> {}
std::string GSReader::removeHeader(int& header,std::string data){
	std::string head = data.substr(1,3);
	size_t lenth = data.find(")))");
	header = textToInteger(head);
	return data.substr(5,lenth-5);
}
std::string GSReader::readBlockUltil(std::string endStr,uint8_t nbStart,uint8_t *uid, uint8_t uidLength){
	std::string data ="";
	for(uint8_t k = nbStart; k < 64 ; k ++){
		if((k+1)%4==0) {
		//khong read because la keyblock
		k++;
		}
		std::string str = readBlock(uid, uidLength, k);
//		ESP_LOGI(REA_TAG, ">>>WE REDING BLOCK %d :'%s'",k,str.c_str());
		if(data.find(endStr)!=std::string::npos){
			data+=str;
//			ESP_LOGI(REA_TAG, "<<<<<<data:'%s'",data.c_str());
			return data;
		}
		data+=str;
//		ESP_LOGI(REA_TAG, "data:'%s'",data.c_str());

	}
	return "READ_ULTIL_FAIL";

}
