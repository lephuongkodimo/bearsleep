/*
 * GSBlock.cpp
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#include "GSBlock.h"

#include <string>
GSBlocks::GSBlocks() {
	// TODO Auto-generated constructor stub
 this->my_BlockName = BLOCKS_NAME::_NO_NAME;
}
GSBlocks::GSBlocks(std::string str) {
	// TODO Auto-generated constructor stub
	this->initBlocks( str) ;
}
GSBlocks::~GSBlocks() {
	// TODO Auto-generated destructor stub
}
/*
 * Call isBlocks before call this fnction
 */
void GSBlocks::initBlocks(std::string str) { //(132,123,123) or 123,132,123;
	if(str.find(";")!=-1){
		str.pop_back();
		str="("+str+")";
	}
	int index = str.find(")");
	str.erase(index, str.size() - 1); //123,123,123
	str.erase(0, 1);
	std::string element = "";
	while (str.find(",") != -1) {
		index = str.find(",");
		std::string element = str.substr(0, index);  // "123,123" -> "123" -> ""
		this->Blocks.push_back(element);
		str.erase(0, index + 1);
	}
	this->Blocks.push_back(str);
	my_BlockName = whoIam(getElement(0));
}
void GSBlocks::setData(std::string str) {
		this->Blocks.push_back(str);
		this->mData = str;
}
std::string GSBlocks::getData() {
	return this->mData;
}
