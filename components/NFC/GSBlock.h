/*
 * GSBlock.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSBLOCK_H_
#define COMPONENTS_GOLIBS_GSBLOCK_H_
#include <string>
#include <vector>
#include "GSUltil.h"
#include "Arduino.h"
#include <BLEAddress.h>
#define BLOCK_BOOK 				"510"
#define BLOCK_INIT				"(1000)"
#define BLOCK_START 			"100"
#define BLOCK_END 				"1000"
#define BLOCK_ENDLOOP			"1001"
#define BLOCK_ROBOT 			"0"
#define BLOCK_TURN 				"210"
#define BLOCK_MOVE 				"200"
#define BLOCK_SOUND 			"300"
#define BLOCK_PIANO 			"301"
#define BLOCK_LIGHT 			"400"
#define BLOCK_TRAFFIC 			"405"
#define BLOCK_DATA 				"7"
#define NULLADDRESS 		((uint8_t*)"\0\0\0\0\0\0")

enum MONTE_BLOCK {
	MONTE_BLOCK_UNKNOW 				= -1,
	MONTE_BLOCK_ROBOT_NAME 		= 0x0000,
	MONTE_BLOCK_ROBOT_NAME_GOBOT,
	MONTE_BLOCK_ROBOT_NAME_TRAFFIC_LIGHT,
	MONTE_BLOCK_ROBOT_NAME_GARABOT,
	MONTE_BLOCK_ROBOT_NAME_MONTEAPP,
	MONTE_BLOCK_BEGIN 			= 0x0100,
	MONTE_BLOCK_MOVE 			= 0x0200,
	MONTE_BLOCK_MOVE_MOVEFORWARD,
	MONTE_BLOCK_MOVE_MOVEBACKWARD,
	MONTE_BLOCK_MOVE_MOVEFORWARD_X2,
	MONTE_BLOCK_MOVE_TURN		= 0x0210,
	MONTE_BLOCK_MOVE_TURN_TURNLEFT,
	MONTE_BLOCK_MOVE_TURN_TURNRIGH,
	MONTE_BLOCK_SOUND 			= 0x0300,
	MONTE_BLOCK_PLAY_PIANO		,
	MONTE_BLOCK_PLAY_PIANO_NOTE_C,
	MONTE_BLOCK_PLAY_PIANO_NOTE_D,
	MONTE_BLOCK_PLAY_PIANO_NOTE_E,
	MONTE_BLOCK_PLAY_PIANO_NOTE_F,
	MONTE_BLOCK_PLAY_PIANO_NOTE_G,
	MONTE_BLOCK_PLAY_PIANO_NOTE_A,
	MONTE_BLOCK_PLAY_PIANO_NOTE_B,
	MONTE_BLOCK_PLAY_PIANO_NOTE_CC,
	MONTE_BLOCK_DISPLAY_LIGHT 	=				0x0400,
	MONTE_BLOCK_DISPLAY_TRAFFICLIGHT =			0x0405,
	MONTE_BLOCK_DISPLAY_TRAFFICLIGHT_AUTO,
	MONTE_BLOCK_DISPLAY_TRAFFICLIGHT_RED,
	MONTE_BLOCK_DISPLAY_TRAFFICLIGHT_YELLOW,
	MONTE_BLOCK_DISPLAY_TRAFFICLIGHT_GREEN,
	MONTE_BLOCK_BOOK =							0x0510,
	MONTE_BLOCK_ANIMATION_DANCE =				0x0900,
	MONTE_BLOCK_STOP 			= 				0x1000,
	MONTE_BLOCK_REPEAT_FOREVER,
};
enum BLOCKS_ROBOT_NAME {
	_GOBOT,
	_GARABOT,
	_TRAFFIC_LIGHTBOT,
	_MONTEAPP,
	_NO_NAME
};
enum BLOCK_TRAFFIC_LIGHT {
	_AUTO,
	_RED,
	_YELLOW,
	_GREEN
};
class GSBlocks {
public:
	GSBlocks();
	GSBlocks(std::string str);
	virtual ~GSBlocks();
	enum BLOCKS_NAME {
		_START,
		_END,
		_ENDLOOP,
		_ROBOT,
		_FUNC,
		_TURNLEFT,
		_TURNRIGHT,
		_MOVEFORWARD,
		_MOVEBACKWARD,
		_SOUND,
		_MUSIC_EN,
		_MUSIC_NOTE,
		_TRAFFIC,
		_BLEADRESS,
		_DATA,
		_BOOK,
		_NO_NAME
	}  ;
	void initBlocks(std::string str);
	BLOCKS_NAME whoIam(){ return my_BlockName;}
	//New cmd for test
	MONTE_BLOCK  whoIamNewCMD(){ return mMonteBlock;}
	std::string to_hex_string(){return to_hexString(mMonteBlock);};
private:BLOCKS_NAME whoIam(std::string KEY_VALUE_BLOCK){
		if(KEY_VALUE_BLOCK == BLOCK_BOOK)			{
				mMonteBlock = MONTE_BLOCK_BOOK;
				return BLOCKS_NAME::_BOOK;
			}
		if(KEY_VALUE_BLOCK == BLOCK_START)			{
			mMonteBlock = MONTE_BLOCK_BEGIN;
			return BLOCKS_NAME::_START;
		}
		if(KEY_VALUE_BLOCK == BLOCK_END) 			{
			mMonteBlock = MONTE_BLOCK_STOP;
			return BLOCKS_NAME::_END;
		}
		if(KEY_VALUE_BLOCK == BLOCK_ENDLOOP) 		{
			mMonteBlock = MONTE_BLOCK_REPEAT_FOREVER;
			return BLOCKS_NAME::_ENDLOOP;
		}
		if(KEY_VALUE_BLOCK == BLOCK_ROBOT) 			return BLOCKS_NAME::_ROBOT;
		if(KEY_VALUE_BLOCK == BLOCK_MOVE) 			{
			if(textToInteger(Blocks[1]) > 0 ){
				mMonteBlock = MONTE_BLOCK_MOVE_MOVEFORWARD;
				return BLOCKS_NAME::_MOVEFORWARD;
				} else {
				mMonteBlock = MONTE_BLOCK_MOVE_MOVEBACKWARD;
				return BLOCKS_NAME::_MOVEBACKWARD;
				}
		}
		if(KEY_VALUE_BLOCK == BLOCK_TURN) 			{
												if(textToInteger(Blocks[1]) > 0 ){
													mMonteBlock = MONTE_BLOCK_MOVE_TURN_TURNLEFT;
													return BLOCKS_NAME::_TURNLEFT;
													} else {
													mMonteBlock = MONTE_BLOCK_MOVE_TURN_TURNRIGH;
													return BLOCKS_NAME::_TURNRIGHT;
													}
												}
		if(KEY_VALUE_BLOCK == BLOCK_SOUND) 			{ int ID = textToInteger(Blocks[1]);

												if(ID==100){
													return BLOCKS_NAME::_MUSIC_EN;
												} else if(ID > 100) {
													return BLOCKS_NAME::_MUSIC_NOTE;
												}
												return BLOCKS_NAME::_SOUND;
												}


		if(Blocks[0] == BLOCK_PIANO){
			int ID = textToInteger(Blocks[1]);

															if(ID==0){
																return BLOCKS_NAME::_MUSIC_EN;
															}else{
																this->IamNote();
																return BLOCKS_NAME::_MUSIC_NOTE;
															}
		}
		if(Blocks[0] == BLOCK_TRAFFIC){

															return BLOCKS_NAME::_TRAFFIC;
				}
		if(Blocks.size()==1) return BLOCKS_NAME::_BLEADRESS;
		if(Blocks[0] == BLOCK_DATA) return BLOCKS_NAME::_DATA;
		return BLOCKS_NAME::_FUNC;
	}
public:
	BLOCK_TRAFFIC_LIGHT getTrafficLight(){
		int ID = textToInteger(Blocks[1]); //000,Y,XXXX
		switch(ID){
		case 0:
			return BLOCK_TRAFFIC_LIGHT::_AUTO;
		case 1:
			return BLOCK_TRAFFIC_LIGHT::_RED;
		case 2:
			return BLOCK_TRAFFIC_LIGHT::_YELLOW;
		case 3:
			return BLOCK_TRAFFIC_LIGHT::_GREEN;
		default :
			return BLOCK_TRAFFIC_LIGHT::_AUTO;
		}
	}
	BLOCKS_ROBOT_NAME getRobotName(){
//		int ID = textToInteger(Blocks[1]); //0,Y,XXXX
		if(Blocks[1]=="G")
			return BLOCKS_ROBOT_NAME::_GOBOT;
		if(Blocks[1]== "A")
			return BLOCKS_ROBOT_NAME::_MONTEAPP;
		if(Blocks[1]== "T")
			return BLOCKS_ROBOT_NAME::_TRAFFIC_LIGHTBOT;
		if(Blocks[1]== "R")
					return BLOCKS_ROBOT_NAME::_GARABOT;
			return BLOCKS_ROBOT_NAME::_NO_NAME;

	}
	note_t IamNote(){
		int ID = textToInteger(Blocks[1]);
		switch(ID){
		case 1:
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_C;
			return NOTE_C;
		case 2:
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_D;
			return NOTE_D;
		case 3:
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_E;
			return NOTE_E;
		case 4:{
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_F;
			return NOTE_F;
		}
		case 5:{
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_G;
			return NOTE_G;
		}
		case 6:{
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_A;
			return NOTE_A;
		}
		case 7:{
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_B;
			return NOTE_B;
		}case 8:{
			mMonteBlock = MONTE_BLOCK_PLAY_PIANO_NOTE_CC;
			return NOTE_MAX;
		}
		default :
			return NOTE_MAX;
		}
	}
	BLEAddress toBLEAdress(){
		if(whoIam()!=BLOCKS_NAME::_BLEADRESS)
			return BLEAddress(NULLADDRESS);
		Blocks[0].insert(10,":");
		Blocks[0].insert(8,":");
		Blocks[0].insert(6,":");
		Blocks[0].insert(4,":");
		Blocks[0].insert(2,":");
		ESP_LOGI("BLOCK","BLEAdress'%s'",Blocks[0].c_str());
		return BLEAddress(Blocks[0]);
	}
	std::string toString(){
		//block chuoi string 100,100,10;
		std::string str = Blocks[0];
        for(int i = 1; i< Blocks.size() ; i ++){
            str = str + "," + Blocks[i];
        }
        str = str + ";";
		return str;
	}
	std::string toStringV(){
		//block chuoi string 100,100,10;
		std::string str = Blocks[0];
		if(str == "210") {
			str = "210;" +Blocks[1] +";0;";
		} else {
		for(int i = 1; i< Blocks.size() ; i ++){
		str = str + ";" + Blocks[i];
		}
		str = str + ";";
		}
		return str;
	}
	std::string getElement(uint8_t i){
		//return phan tu thu i trong block
		return Blocks[i];
	}
	void addElement(std::string str){
		Blocks.push_back(str);
	}
	void repalce(std::string re, int stt){
		Blocks[stt].clear();
		Blocks[stt]=re;
	}
	int Size(){
		//munber  element
		return Blocks.size();
	}
	void setData(std::string str);
	std::string getData();
private:
	MONTE_BLOCK mMonteBlock;
	std::string mData;
	BLOCKS_NAME my_BlockName;
	std::vector<std::string> Blocks;


};


#endif /* COMPONENTS_GOLIBS_GSBLOCK_H_ */
