/*
 * GSReader.h
 *
 *  Created on: Oct 4, 2018
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSREADER_H_
#define COMPONENTS_GOLIBS_GSREADER_H_
#include "Adafruit_PN532.h"
#include <vector>
#include <sstream>
#include <string>
#include <esp_log.h>
#include "Arduino.h"
#include <esp_log.h>
#include "GSBlock.h"
#define USE_SPI_PN532
#define GPIO_NFC_CS			GPIO_NUM_5
#define GPIO_NFC_CLK		GPIO_NUM_18
#define GPIO_NFC_MOSI		GPIO_NUM_23
#define GPIO_NFC_MISO		GPIO_NUM_19
#define PN532_SCK  (GPIO_NFC_CLK)
#define PN532_MOSI (GPIO_NFC_MOSI)
#define PN532_SS   (GPIO_NFC_CS)
#define PN532_MISO (GPIO_NFC_MISO)
extern bool GSg_writeTag;
extern bool GSg_testTag;
static const char *REA_TAG = "GSReader";
// If using the breakout with SPI, define the pins for SPI communication.


// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (33)
#define PN532_RESET (13)  // Not connected by default on the NFC Shield

// Uncomment just _one_ line below depending on how your breakout or shield
// is connected to the Arduino:

// Use this line for a brea+kout with a software SPI connection (recommended):
typedef struct dataBook_t{

	std::string story="";
	std::string lesson = "";
	std::string unit = "";
	std::string slution = "";
	dataBook_t& operator=(const dataBook_t& msg){
		this->lesson = msg.lesson;
		this->slution = msg.slution;
		this->story = msg.story;
		this->unit = msg.unit;
	return *this;
	}
}dataBook_t;
class GSReaderCallBacks;
//Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

class GSReader {
	Adafruit_PN532 nfc;
public:
	GSReader() :
#ifdef USE_SPI_PN532
			nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS)
#else
nfc(PN532_IRQ, PN532_RESET)
#endif
{

	}
	;

	virtual ~GSReader();

	void init();
	void run();

	void stop();
	void setCallBacks(GSReaderCallBacks *mGSReaderCallBack) {
		mGSReaderCallBacks = mGSReaderCallBack;
	}


;
private:
	//for book
	void getBookFromBlock(int nbStart,uint8_t *uid, uint8_t uidLength);
	std::string removeHeader(int& header,std::string data);
	std::string getBookData(int nbStart,uint8_t *uid, uint8_t uidLength);
	std::string readBlockUltil(std::string endStr,uint8_t nbStart,uint8_t *uid, uint8_t uidLength);

	bool isBlocks(std::string strTag);//is blocks?
	void write(std::string str,uint8_t blockNumber,uint8_t *uid, uint8_t uidLength);
	std::string read(uint8_t *uid, uint8_t uidLength);
	std::string readBlock(uint8_t *uid, uint8_t uidLength, uint8_t blockNumber);
	GSReaderCallBacks *mGSReaderCallBacks;
	std::vector<uint8_t> vuid_last;
	std::vector<uint8_t> vuid;
	uint8_t _clk, _miso, _mosi, _ss;
	uint8_t cdetec = 0;		//dem so lan xuat hien block
	int count_data_bool = 0;
	uint8_t _KeyaOld[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
	uint8_t _Keya[6] = { 0x19, 0x09, 0x5F, 0x4b, 0x44, 0x4d };
};

static void offNFC(){
	ESP_LOGI("GSReader", "OFF NFC");
	digitalWrite(GPIO_NFC_CLK, LOW); //low == kick on
	digitalWrite(GPIO_NFC_CS, LOW); //low == kick on
	digitalWrite(GPIO_NFC_MISO, LOW); //low == kick on
	digitalWrite(GPIO_NFC_MOSI, LOW); //low == kick on
}


class GSReaderCallBacks {
public:
	GSReaderCallBacks() {

	}
	virtual ~GSReaderCallBacks() {

	}

//	virtual void onBlock() = 0;
	virtual void onRead(GSBlocks m_GSBlock) =0;
	virtual void onRobot(BLOCKS_ROBOT_NAME type,BLEAddress address) = 0;
	virtual void onBook(GSBlocks m_GSBlock) = 0 ;

};

#endif /* COMPONENTS_GOLIBS_GSREADER_H_ */
