/*
 * GSUltil.h
 *
 *  Created on: Feb 19, 2019
 *      Author: Admin
 */

#ifndef COMPONENTS_GOLIBS_GSULTIL_H_
#define COMPONENTS_GOLIBS_GSULTIL_H_

#include <sstream>
#include <string>

static int textToInteger(std::string str){
	int number;
	std::istringstream iss (str);
	iss >> number;
	return number;
}
static std::string to_string(int i) {
	std::stringstream ss;
	ss << i;
	return ss.str();
}
//ex 0xFFF ->'FFF'
static std::string to_hexString(int d){
	std::stringstream stream;
	stream << std::hex << d;
	std::string result( stream.str() );
	return result;
}
static int noteToInt(int n){
		switch(n){
		case 0:
			return 1;
		case 2:
			return 2;
		case 4:
			return 3;
		case 5:
			return 4;
		case 7:
			return 5;
		case 9:
			return 6;
		case 11:
			return 7;
		default :
			return 8;
		}

}
#endif /* COMPONENTS_GOLIBS_GSULTIL_H_ */
